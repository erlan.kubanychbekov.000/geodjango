## GeoDjango

### Setting up the project to run

1 Set up a virtual environment and activate it:
```` shell
python -m venv venv
. venv/bin/activate
````

2 Install dependencies:
```` shell
pip install -r requirments.txt
````

3 Run the database using Docker. To do this, you should have [Docker](https://docs.docker.com/engine/install/) installed.
Alternatively, you can install PostgreSQL and create a database, but we will cover setting up the database using [Docker](https://docs.docker.com/engine/install/).
```shell

docker run --name=geo-db -d -e POSTGRES_USER=geo -e POSTGRES_PASSWORD=secret -e POSTGRES_DB=geo_db -p 5434:5432 postgis/postgis

```

4 Create a ``.env`` file and fill it with the following data:
``` text
SECRET_KEY=<generate a unique key>
DB_NAME=<specify the database name>
DB_USER=<specify the database user>
DB_PASSWORD=<specify the database password>
DB_HOST=<specify the database host>
DB_PORT=<specify the database port>
```
Alternatively, you can copy the data from the ``.env.dist`` file; the information from step 3 is there.

5 Start project command:
```shell
python manage.py runserver
```
The service will be accessible at http://127.0.0.1:8000/

### You can run the project using docker-compose. ``docker-compose``
In this case, you will need to make some adjustments to the ``.env`` file. You can find an example in the ``.env.dist-dc`` file.

For running the project with Docker Compose, you should have [Docker](https://docs.docker.com/engine/install/) installed.

To start the project, run:
```shell
docker-compose up --build
```
The service will be accessible at http://127.0.0.1:1111/.

To stop Docker, enter:
```shell
docker-compose down
```

### Creating a Superuser
If you've started without Docker, simply enter:
```shell
python manage.py createsuperuser
```
To create a superuser if you've started via ``docker-compose``, follow these steps:
Enter the Docker shell:
```shell
docker-compose exec geo sh
```
Enter the command:
```shell
python manage.py createsuperuser
```
### If you followed the instructions, everything should be up and running now.
