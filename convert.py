import os
import rasterio

input_directory = "jp2_files"
output_directory = "tiff_files"


os.makedirs(output_directory, exist_ok=True)

for root, _, files in os.walk(input_directory):
    for jp2_file in files:
        if jp2_file.endswith(".jp2"):
            input_file_path = os.path.join(root, jp2_file)
            relative_path = os.path.relpath(input_file_path, input_directory)
            output_file_path = os.path.join(output_directory, relative_path.replace(".jp2", ".tif"))

            with rasterio.open(input_file_path) as src:
                profile = src.profile
                data = src.read()

                profile["driver"] = "GTiff"
                with rasterio.open(output_file_path, "w", **profile) as dst:
                    dst.write(data)

            print(f"Converted {jp2_file} to {output_file_path}")
