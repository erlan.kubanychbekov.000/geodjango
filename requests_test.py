import requests
from conf import email, password
from django.contrib.gis.geos import Polygon
from sentinel2_downloader import SentinelAPI

url = "https://catalogue.dataspace.copernicus.eu/odata/v1/Products?$filter=Name eq 'S2A_MSIL2A_20230816T055641_N0509_R091_T43TDH_20230816T110301.SAFE'"
response = requests.get(url)
json_data = response.json()
coordinates = json_data['value'][0]['GeoFootprint']['coordinates']
polygon = Polygon(coordinates[0])
api = SentinelAPI(username=email, password=password)

products = api.query(footprint=str(polygon),
                     start_date='2020-08-17', end_date='2020-08-18', cloud_cover_percentage='20', product_type='MSIL1C',
                     platform_name='SENTINEL-2')
if len(products) > 1:
    for product in products:
        a = api.download(product_id=product['Id'], directory_path='output')
