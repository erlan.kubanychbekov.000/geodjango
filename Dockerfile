FROM python:3.8

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1


RUN apt-get update && \
    apt-get install -y python3-gdal

COPY . /Region
WORKDIR /Region

RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /Region/src
