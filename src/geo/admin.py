import json
from django.http import HttpResponse
from django.contrib import admin

from django.contrib.auth.models import Group, User
from leaflet.admin import LeafletGeoAdmin
from .models import (
    Region,
    District,
    Canton,
    Contour
)


class ExportFile:
    export_file_fields = None

    @admin.action(description="Export JSON file")
    def export_json_file(self, request, queryset):
        """Export Json file function"""
        data = []
        for obj in queryset:
            coordinates = obj.geometry.coords
            json_data = {}
            if self.export_file_fields is not None:
                for field in self.export_file_fields:
                    field_value = str(getattr(obj, field))
                    json_data[field] = field_value

            if obj.title:
                json_data["type"] = obj._meta.model_name
                json_data["coordinates"] = coordinates
                data.append(json_data)

        json_string = json.dumps(data, ensure_ascii=False, indent=2)

        response = HttpResponse(json_string, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="exported_data.json"'

        return response


@admin.register(Region)
class RegionAdmin(LeafletGeoAdmin, ExportFile):
    """Region admin panel config"""
    list_display = ("id", "title", "date_created")
    search_fields = ("title",)
    list_filter = ("date_created",)
    ordering = ("id", "date_created")
    list_display_links = ("id", "title")
    actions = ["export_json_file"]
    export_file_fields = ["title"]


@admin.register(District)
class DistrictAdmin(LeafletGeoAdmin, ExportFile):
    """District admin panel config"""
    list_display = ("id", "title", "get_region_title", "date_created")
    list_filter = ("date_created", "region__title")
    search_fields = ("region__title", "title")
    ordering = ("id", "date_created", "region__title")
    list_display_links = ("id", "title")
    actions = ["export_json_file"]
    export_file_fields = ["title", "region"]

    def get_region_title(self, obj):
        return obj.region.title if obj.region else None


@admin.register(Canton)
class CantonAdmin(LeafletGeoAdmin, ExportFile):
    """Canton admin panel config"""
    list_display = ("id", "title", "get_district_title", "date_created")
    list_filter = ("district__title", "date_created")
    search_fields = ("title", "district__title")
    ordering = ("id", "date_created", "district__title")
    list_display_links = ("id", "title")
    actions = ["export_json_file"]
    export_file_fields = ["title", "district"]

    def get_district_title(self, obj):
        return obj.district.title if obj.district else None


@admin.register(Contour)
class ContourAdmin(LeafletGeoAdmin):
    """Contour admin panel config"""
    list_display = ("id", "title", "date_created")
    list_filter = ("canton__title", "date_created")
    search_fields = ("title", "canton__title")
    ordering = ("id", "date_created", "canton__title")
    list_display_links = ("id", "title")
    gis_widget_kwargs = {'attrs':
                             {'default_lon': 75, 'default_lat': 41, 'default_zoom': 6}
                         }


admin.site.unregister(Group)
admin.site.unregister(User)
