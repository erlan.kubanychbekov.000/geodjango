from django.shortcuts import render
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.views import APIView
from .models import Contour, Region, District
from .serializers import ContourSerializer, GeoFeatureSerializer
from .filters import ContourFilterSet
from mixins.gei_mixins import BaseGeoJsonView
import requests


class ContourView(ListAPIView):
    """
    API endpoint getting a list of contours and filtering
    """
    queryset = Contour.objects.all()
    serializer_class = ContourSerializer
    filterset_class = ContourFilterSet


class RegionView(BaseGeoJsonView):
    """
    API endpoint getting GeoJson Regions
    """
    model = Region
    serializer_class = GeoFeatureSerializer


class TestAPIView(APIView):
    def get(self, request):
        start_date = '2023-08-16'
        end_date = '2023-08-17'
        region = District.objects.first()
        params = {
                '$filter': f"""OData.CSC.Intersects(area=geography'{region.geometry}')
                            and ContentDate/Start gt {start_date}T00:00:00.000Z and
                            ContentDate/Start lt {end_date}T00:00:00.000Z and
                            Collection/Name eq 'SENTINEL-2' and
                            Attributes/OData.CSC.DoubleAttribute/any(att:att/Name eq 'cloudCover' and
                            att/OData.CSC.DoubleAttribute/Value lt 20) and 
                            contains(Name,'MSIL1C')""",
                '$orderby': 'ContentDate/Start'
            }
        print(params)
        response = requests.get('https://catalogue.dataspace.copernicus.eu/odata/v1/Products', params=params)
        print(response.content)


