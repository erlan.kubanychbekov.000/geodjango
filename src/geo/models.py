from django.contrib.gis.db import models


class AbstractGis(models.Model):
    """Abstract class, for creating gis models"""
    title = models.CharField(max_length=50)
    geometry = models.PolygonField()
    date_created = models.DateField(auto_now=True)

    class Meta:
        abstract = True


class Region(AbstractGis):
    """Model Region"""
    pass

    def __str__(self):
        return self.title


class District(AbstractGis):
    """Model District"""
    region = models.ForeignKey(Region, related_name="districts", on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Canton(AbstractGis):
    """Model Canton"""
    district = models.ForeignKey(District, related_name="cantons", on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Contour(AbstractGis):
    """Model Contour"""
    canton = models.ForeignKey(Canton, related_name="contours", on_delete=models.CASCADE)
    title = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.title


from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)

    def __str__(self):
        return self.name
