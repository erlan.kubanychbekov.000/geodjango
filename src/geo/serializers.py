from rest_framework import serializers
from .models import Contour, Region


class ContourSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contour
        fields = "__all__"


class GeometrySerializer(serializers.Serializer):
    coords = serializers.ListField(child=serializers.CharField())
    type = serializers.CharField(default="Point")


class GeoFeatureSerializer(serializers.Serializer):
    type = serializers.CharField(default="Feature")
    geometry = GeometrySerializer()

    def to_representation(self, instance):
        geo_representation = super().to_representation(instance)
        geo_representation['geometry']['coordinates'] = instance.geometry.coords
        geo_representation['geometry']['type'] = instance.geometry.geom_type
        geo_representation['properties'] = {}
        del geo_representation['geometry']['coords']
        return geo_representation
