import json
from django.test import TestCase
from django.contrib.gis.geos import Polygon
from rest_framework.test import APIClient
from .models import Contour, Canton


class YourModelAPITest(TestCase):

    def setUp(self):
        self.client = APIClient()
        coordinates = [
            (78.14033331049438, 42.50010295549245),
            (78.14033331049438, 42.479821087046076),
            (78.17549056528236, 42.479821087046076),
            (78.17549056528236, 42.50010295549245),
            (78.14033331049438, 42.50010295549245)
        ]
        geometry = Polygon(coordinates)
        self.test_contour = Contour.objects.create(
            title='test_contour',
            geometry=geometry,
            canton_id=1
        )

    def get_json_data(self, response):
        return json.loads(response.content.decode('utf-8'))

    def test_get_geo_json_regions(self):
        response = self.client.get(f'/geo/regions/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.status_code, 200)
        data = self.get_json_data(response)

        self.assertEqual(data['type'], 'FeatureCollection')

        features = data['features']
        self.assertIsInstance(features, list)

        for feature in features:
            self.assertEqual(feature['type'], 'Feature')

            geometry = feature['geometry']
            self.assertIsInstance(geometry, dict)

            self.assertEqual(geometry['type'], 'Polygon')

            coordinates = geometry['coordinates']
            self.assertIsInstance(coordinates, list)

    def test_filter_contour(self):
        canton = Canton.objects.get(id=1)
        response = self.client.get(f'/geo/contours/', data={'canton_title': canton.title})
        self.assertEqual(response.status_code, 200)
        data = self.get_json_data(response)
        self.assertEqual(data[0]['title'], 'test_contour')
