from django.urls import path
from .views import (
    ContourView,
    RegionView,
    TestAPIView
)

urlpatterns = [
    path("contours/", ContourView.as_view(), name="contour"),
    path("regions/", RegionView.as_view(), name="regions"),
    path("test/", TestAPIView.as_view(), name="test")
]
