from django_filters import rest_framework as filters
from .models import Contour


class ContourFilterSet(filters.FilterSet):
    canton_title = filters.CharFilter(field_name='canton__title', lookup_expr='icontains')
    district_title = filters.CharFilter(field_name='canton__district__title', lookup_expr='icontains')
    region_title = filters.CharFilter(field_name='canton__district__region__title', lookup_expr='icontains')

    class Meta:
        model = Contour
        fields = ['canton_title', 'district_title', 'region_title']
