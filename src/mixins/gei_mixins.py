from rest_framework.response import Response
from rest_framework.views import APIView
from geo.serializers import GeoFeatureSerializer


class BaseGeoJsonView(APIView):
    model = None
    serializer_class = GeoFeatureSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.model.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        geo_data = {
            'type': 'FeatureCollection',
            'features': serializer.data
        }
        return Response(geo_data)
